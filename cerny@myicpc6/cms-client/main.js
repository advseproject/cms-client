(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _wlayout_wlayout_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./wlayout/wlayout.component */ "./src/app/wlayout/wlayout.component.ts");
/* harmony import */ var _conf_wrapper_conf_wrapper_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./conf-wrapper/conf-wrapper.component */ "./src/app/conf-wrapper/conf-wrapper.component.ts");
/* harmony import */ var _exam_assignment_exam_assignment_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./exam-assignment/exam-assignment.component */ "./src/app/exam-assignment/exam-assignment.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    { path: '', component: _wlayout_wlayout_component__WEBPACK_IMPORTED_MODULE_2__["WlayoutComponent"], children: [
            { path: '', component: _conf_wrapper_conf_wrapper_component__WEBPACK_IMPORTED_MODULE_3__["ConfWrapperComponent"] },
            { path: 'exam', component: _exam_assignment_exam_assignment_component__WEBPACK_IMPORTED_MODULE_4__["ExamAssignmentComponent"] }
        ]
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"wrapper\" class=\"content\">\n  <router-outlet></router-outlet>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var keycloak_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! keycloak-angular */ "./node_modules/keycloak-angular/fesm5/keycloak-angular.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


var AppComponent = /** @class */ (function () {
    function AppComponent(keycloakService) {
        this.keycloakService = keycloakService;
        this.title = 'base-app';
        this.keycloakService.getToken().then(function (token) {
            localStorage.setItem("token", token);
        });
    }
    AppComponent.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        console.log("on init");
                        return [4 /*yield*/, this.keycloakService.isLoggedIn()];
                    case 1:
                        if (!_b.sent()) return [3 /*break*/, 3];
                        _a = this;
                        return [4 /*yield*/, this.keycloakService.loadUserProfile()];
                    case 2:
                        _a.userDetails = _b.sent();
                        _b.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    AppComponent.prototype.doLogout = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.keycloakService.logout()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [keycloak_angular__WEBPACK_IMPORTED_MODULE_1__["KeycloakService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var keycloak_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! keycloak-angular */ "./node_modules/keycloak-angular/fesm5/keycloak-angular.js");
/* harmony import */ var _utils_app_init__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./utils/app-init */ "./src/app/utils/app-init.ts");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./auth.guard */ "./src/app/auth.guard.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _wlayout_wlayout_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./wlayout/wlayout.component */ "./src/app/wlayout/wlayout.component.ts");
/* harmony import */ var _navigation_navigation_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./navigation/navigation.component */ "./src/app/navigation/navigation.component.ts");
/* harmony import */ var _exam_assignment_exam_assignment_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./exam-assignment/exam-assignment.component */ "./src/app/exam-assignment/exam-assignment.component.ts");
/* harmony import */ var _conf_wrapper_conf_wrapper_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./conf-wrapper/conf-wrapper.component */ "./src/app/conf-wrapper/conf-wrapper.component.ts");
/* harmony import */ var _conf_list_conf_list_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./conf-list/conf-list.component */ "./src/app/conf-list/conf-list.component.ts");
/* harmony import */ var _conf_group_conf_group_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./conf-group/conf-group.component */ "./src/app/conf-group/conf-group.component.ts");
/* harmony import */ var _conf_creator_conf_creator_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./conf-creator/conf-creator.component */ "./src/app/conf-creator/conf-creator.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _wlayout_wlayout_component__WEBPACK_IMPORTED_MODULE_8__["WlayoutComponent"],
                _navigation_navigation_component__WEBPACK_IMPORTED_MODULE_9__["NavigationComponent"],
                _exam_assignment_exam_assignment_component__WEBPACK_IMPORTED_MODULE_10__["ExamAssignmentComponent"],
                _conf_wrapper_conf_wrapper_component__WEBPACK_IMPORTED_MODULE_11__["ConfWrapperComponent"],
                _conf_list_conf_list_component__WEBPACK_IMPORTED_MODULE_12__["ConfListComponent"],
                _conf_group_conf_group_component__WEBPACK_IMPORTED_MODULE_13__["ConfGroupComponent"],
                _conf_creator_conf_creator_component__WEBPACK_IMPORTED_MODULE_14__["ConfCreatorComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                keycloak_angular__WEBPACK_IMPORTED_MODULE_4__["KeycloakAngularModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_15__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_15__["ReactiveFormsModule"],
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_16__["NgSelectModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"]
            ],
            providers: [_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"], {
                    provide: _angular_core__WEBPACK_IMPORTED_MODULE_1__["APP_INITIALIZER"],
                    useFactory: _utils_app_init__WEBPACK_IMPORTED_MODULE_5__["initializer"],
                    multi: true,
                    deps: [keycloak_angular__WEBPACK_IMPORTED_MODULE_4__["KeycloakService"]]
                }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth.guard.ts":
/*!*******************************!*\
  !*** ./src/app/auth.guard.ts ***!
  \*******************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var keycloak_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! keycloak-angular */ "./node_modules/keycloak-angular/fesm5/keycloak-angular.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var AuthGuard = /** @class */ (function (_super) {
    __extends(AuthGuard, _super);
    function AuthGuard(router, keycloakAngular) {
        var _this = _super.call(this, router, keycloakAngular) || this;
        _this.router = router;
        _this.keycloakAngular = keycloakAngular;
        return _this;
    }
    AuthGuard.prototype.isAccessAllowed = function (route, state) {
        var _this = this;
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var requiredRoles, granted, _i, requiredRoles_1, requiredRole;
            return __generator(this, function (_a) {
                if (!this.authenticated) {
                    this.keycloakAngular.login();
                    return [2 /*return*/];
                }
                console.log('role restriction given at app-routing.module for this route', route.data.roles);
                console.log('User roles coming after login from keycloak :', this.roles);
                requiredRoles = route.data.roles;
                granted = false;
                if (!requiredRoles || requiredRoles.length === 0) {
                    granted = true;
                }
                else {
                    for (_i = 0, requiredRoles_1 = requiredRoles; _i < requiredRoles_1.length; _i++) {
                        requiredRole = requiredRoles_1[_i];
                        if (this.roles.indexOf(requiredRole) > -1) {
                            granted = true;
                            break;
                        }
                    }
                }
                if (granted === false) {
                    this.router.navigate(['/']);
                }
                resolve(granted);
                return [2 /*return*/];
            });
        }); });
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], keycloak_angular__WEBPACK_IMPORTED_MODULE_2__["KeycloakService"]])
    ], AuthGuard);
    return AuthGuard;
}(keycloak_angular__WEBPACK_IMPORTED_MODULE_2__["KeycloakAuthGuard"]));



/***/ }),

/***/ "./src/app/conf-creator/conf-creator.component.html":
/*!**********************************************************!*\
  !*** ./src/app/conf-creator/conf-creator.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"columns\">\n  <div class=\"column\">\n\n    <form [formGroup]=\"groupForm\" (ngSubmit)=\"onSubmit()\" novalidate>\n\n      <div class=\"field\">\n        <label class=\"label\">Category</label>\n        <div class=\"control\">\n          <ng-select [items]=\"configInfo\"\n                     bindLabel=\"name\"\n                     autofocus\n                     bindValue=\"id\"\n                     placeholder=\"Select Category\"\n                     formControlName=\"category\" (change)=\"groupSelected($event)\">\n            <ng-template ng-option-tmp let-item=\"item\" let-index=\"index\" let-search=\"id\">\n              <b>{{item.name}}</b> | {{item.id}}\n              <p>{{item.description}}</p>\n            </ng-template>\n          </ng-select>\n        </div>\n      </div>\n\n      <div class=\"field\" *ngIf=\"showLevel\">\n        <label class=\"label\">Level</label>\n        <div class=\"control\">\n          <ng-select [items]=\"selectedLevels\"\n                     bindLabel=\"name\"\n                     autofocus\n                     bindValue=\"id\"\n                     placeholder=\"Select Level\"\n                     formControlName=\"level\" (change)=\"levelSelected($event)\">\n          </ng-select>\n        </div>\n      </div>\n\n\n      <div class=\"field\" *ngIf=\"showLanguage\">\n        <label class=\"label\">Language</label>\n        <div class=\"control\">\n          <ng-select [items]=\"selectedLanguages\"\n                     bindLabel=\"language\"\n                     autofocus\n                     bindValue=\"language\"\n                     placeholder=\"Select Language\"\n                     formControlName=\"language\" (change)=\"languageSelected($event)\">\n          </ng-select>\n        </div>\n      </div>\n\n\n      <div class=\"field\" *ngIf=\"showCount\">\n        <label class=\"label\">Count</label>\n        <div class=\"control\">\n          <ng-select [items]=\"selectedCount\"\n                     bindLabel=\"id\"\n                     autofocus\n                     bindValue=\"name\"\n                     placeholder=\"Select Count\"\n                     formControlName=\"count\">\n          </ng-select>\n        </div>\n      </div>\n\n      <div class=\"field is-grouped\">\n        <div class=\"control\">\n          <button class=\"button is-link\" type=\"submit\">Add group</button>\n        </div>\n        <div class=\"control\">\n          <button class=\"button is-text\" type=\"reset\" (click)=\"revert()\">Revert</button>\n        </div>\n      </div>\n\n    </form>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/conf-creator/conf-creator.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/conf-creator/conf-creator.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbmYtY3JlYXRvci9jb25mLWNyZWF0b3IuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/conf-creator/conf-creator.component.ts":
/*!********************************************************!*\
  !*** ./src/app/conf-creator/conf-creator.component.ts ***!
  \********************************************************/
/*! exports provided: ConfCreatorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfCreatorComponent", function() { return ConfCreatorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ConfCreatorComponent = /** @class */ (function () {
    function ConfCreatorComponent() {
        this.group = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ConfCreatorComponent.prototype.ngOnInit = function () {
        this.groupForm = this.createFormGroup();
        this.initVariables();
    };
    ConfCreatorComponent.prototype.initVariables = function () {
        this.showLevel = false;
        this.showLanguage = false;
        this.showCount = false;
        this.selectedQuestionCountDtos = null;
        this.selectedLevels = new Array();
        this.selectedLanguages = new Array();
        this.selectedCount = new Array();
    };
    ConfCreatorComponent.prototype.createFormGroup = function () {
        return new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            category: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            level: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            language: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            count: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
        });
    };
    ConfCreatorComponent.prototype.groupSelected = function (event) {
        var _this = this;
        this.selectedQuestionCountDtos = event.questionCountDtos;
        var levels = new Array();
        this.selectedQuestionCountDtos.forEach(function (q) {
            levels.push(q.level);
        });
        var filteredLevels = levels.filter(function (x, i, a) { return x && a.indexOf(x) === i; });
        this.selectedLevels = new Array();
        filteredLevels.map(function (n) {
            console.log(n);
            _this.selectedLevels.push({ id: n, name: n });
        });
        this.showLevel = true;
        this.groupForm.controls["level"].patchValue(null);
        this.groupForm.controls["language"].patchValue(null);
        this.groupForm.controls["count"].patchValue(null);
    };
    ConfCreatorComponent.prototype.levelSelected = function (event) {
        this.selectedLanguages = this.selectedQuestionCountDtos.filter(function (dto) { return dto.level == event.id; });
        this.groupForm.controls["language"].patchValue(null);
        this.groupForm.controls["count"].patchValue(null);
        this.showLanguage = true;
    };
    ConfCreatorComponent.prototype.languageSelected = function (event) {
        this.selectedCount = new Array();
        console.log(event);
        var count = this.selectedLanguages.find(function (dto) { return dto.language == event.language; });
        console.log(count);
        for (var i = 1; i <= count.count; i++) {
            this.selectedCount.push({ id: i, name: i });
        }
        this.groupForm.controls["count"].patchValue(null);
        this.showCount = true;
        console.log(this.selectedCount);
    };
    ConfCreatorComponent.prototype.onSubmit = function () {
        var result = Object.assign({}, this.groupForm.value);
        this.group.emit(result);
        alert("Group created");
        this.groupForm.reset();
        this.initVariables();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ConfCreatorComponent.prototype, "configInfo", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], ConfCreatorComponent.prototype, "group", void 0);
    ConfCreatorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-conf-creator',
            template: __webpack_require__(/*! ./conf-creator.component.html */ "./src/app/conf-creator/conf-creator.component.html"),
            styles: [__webpack_require__(/*! ./conf-creator.component.scss */ "./src/app/conf-creator/conf-creator.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ConfCreatorComponent);
    return ConfCreatorComponent;
}());



/***/ }),

/***/ "./src/app/conf-group/conf-group.component.html":
/*!******************************************************!*\
  !*** ./src/app/conf-group/conf-group.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"margin-top: 20px\">\n  <div class=\"box\" *ngFor=\"let g of groups\">\n    <article class=\"media\">\n\n      <div class=\"media-content\">\n        <div class=\"content\">\n          <p>Category ID: {{g.category}} | Level: {{g.level}} | Language: {{g.language}} | Count: {{g.count}}</p>\n        </div>\n      </div>\n    </article>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/conf-group/conf-group.component.scss":
/*!******************************************************!*\
  !*** ./src/app/conf-group/conf-group.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbmYtZ3JvdXAvY29uZi1ncm91cC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/conf-group/conf-group.component.ts":
/*!****************************************************!*\
  !*** ./src/app/conf-group/conf-group.component.ts ***!
  \****************************************************/
/*! exports provided: ConfGroupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfGroupComponent", function() { return ConfGroupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ConfGroupComponent = /** @class */ (function () {
    function ConfGroupComponent() {
    }
    ConfGroupComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ConfGroupComponent.prototype, "groups", void 0);
    ConfGroupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-conf-group',
            template: __webpack_require__(/*! ./conf-group.component.html */ "./src/app/conf-group/conf-group.component.html"),
            styles: [__webpack_require__(/*! ./conf-group.component.scss */ "./src/app/conf-group/conf-group.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ConfGroupComponent);
    return ConfGroupComponent;
}());



/***/ }),

/***/ "./src/app/conf-list/conf-list.component.html":
/*!****************************************************!*\
  !*** ./src/app/conf-list/conf-list.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  conf-list works!\n</p>\n"

/***/ }),

/***/ "./src/app/conf-list/conf-list.component.scss":
/*!****************************************************!*\
  !*** ./src/app/conf-list/conf-list.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbmYtbGlzdC9jb25mLWxpc3QuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/conf-list/conf-list.component.ts":
/*!**************************************************!*\
  !*** ./src/app/conf-list/conf-list.component.ts ***!
  \**************************************************/
/*! exports provided: ConfListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfListComponent", function() { return ConfListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ConfListComponent = /** @class */ (function () {
    function ConfListComponent() {
    }
    ConfListComponent.prototype.ngOnInit = function () {
    };
    ConfListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-conf-list',
            template: __webpack_require__(/*! ./conf-list.component.html */ "./src/app/conf-list/conf-list.component.html"),
            styles: [__webpack_require__(/*! ./conf-list.component.scss */ "./src/app/conf-list/conf-list.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ConfListComponent);
    return ConfListComponent;
}());



/***/ }),

/***/ "./src/app/conf-wrapper/conf-wrapper.component.html":
/*!**********************************************************!*\
  !*** ./src/app/conf-wrapper/conf-wrapper.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" style=\"padding: 10px;\">\n\n  <h3>Create Configuration</h3>\n\n\n  <div class=\"columns\">\n    <div class=\"column\">\n      <h4>Add Description</h4>\n\n\n      <form [formGroup]=\"configForm\" novalidate>\n\n        <div class=\"field\">\n          <label class=\"label\">Name</label>\n          <div class=\"control\">\n            <input class=\"input\" type=\"text\" placeholder=\"Name\" formControlName=\"name\">\n          </div>\n        </div>\n\n        <div class=\"field\">\n          <label class=\"label\">Description</label>\n          <div class=\"control\">\n            <textarea class=\"textarea\" placeholder=\"Description\" formControlName=\"description\"></textarea>\n          </div>\n        </div>\n\n      </form>\n\n    </div>\n\n    <div class=\"column\">\n      <h4>Add Groups</h4>\n      <app-conf-creator [configInfo]=\"configInfo\" (group)=\"passGroup($event)\"></app-conf-creator>\n    </div>\n  </div>\n\n  <div class=\"columns\">\n    <div class=\"column\">\n\n      <app-conf-group [groups]=\"groups\"></app-conf-group>\n\n\n      <hr>\n\n      <div class=\"field is-grouped\">\n        <div class=\"control\">\n          <button class=\"button is-success\" type=\"submit\" (click)=\"onSubmit()\">Save Configuration</button>\n        </div>\n        <div class=\"control\">\n          <button class=\"button is-text\" type=\"reset\" (click)=\"revert()\">Revert</button>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n\n\n\n\n</div>\n"

/***/ }),

/***/ "./src/app/conf-wrapper/conf-wrapper.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/conf-wrapper/conf-wrapper.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbmYtd3JhcHBlci9jb25mLXdyYXBwZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/conf-wrapper/conf-wrapper.component.ts":
/*!********************************************************!*\
  !*** ./src/app/conf-wrapper/conf-wrapper.component.ts ***!
  \********************************************************/
/*! exports provided: ConfWrapperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfWrapperComponent", function() { return ConfWrapperComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _configuration_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../configuration.service */ "./src/app/configuration.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ConfWrapperComponent = /** @class */ (function () {
    function ConfWrapperComponent(config) {
        this.config = config;
        this.groups = new Array();
    }
    ConfWrapperComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.config.getConfigInfo().subscribe(function (data) {
            console.log(data);
            _this.configInfo = data;
        });
        this.configForm = this.createFormGroup();
    };
    ConfWrapperComponent.prototype.passGroup = function (group) {
        console.log(group);
        this.groups.push(group);
    };
    ConfWrapperComponent.prototype.createFormGroup = function () {
        return new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]()
        });
    };
    ConfWrapperComponent.prototype.onSubmit = function () {
        var _this = this;
        var result = Object.assign({}, this.configForm.value);
        this.config.createConfig({
            name: result.name,
            description: result.description,
            groups: this.groups
        }).subscribe(function (data) {
            alert("Configuration created");
            _this.configForm.reset();
            _this.groups = [];
        });
    };
    ConfWrapperComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-conf-wrapper',
            template: __webpack_require__(/*! ./conf-wrapper.component.html */ "./src/app/conf-wrapper/conf-wrapper.component.html"),
            styles: [__webpack_require__(/*! ./conf-wrapper.component.scss */ "./src/app/conf-wrapper/conf-wrapper.component.scss")]
        }),
        __metadata("design:paramtypes", [_configuration_service__WEBPACK_IMPORTED_MODULE_1__["ConfigurationService"]])
    ], ConfWrapperComponent);
    return ConfWrapperComponent;
}());



/***/ }),

/***/ "./src/app/configuration.service.ts":
/*!******************************************!*\
  !*** ./src/app/configuration.service.ts ***!
  \******************************************/
/*! exports provided: ConfigurationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigurationService", function() { return ConfigurationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var keycloak_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! keycloak-angular */ "./node_modules/keycloak-angular/fesm5/keycloak-angular.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ConfigurationService = /** @class */ (function () {
    function ConfigurationService(http, keycloakService) {
        this.http = http;
        this.keycloakService = keycloakService;
        this.CMS_URL = "http://cms-backend.myicpc.live/";
        this.EMS_URL = "http://ems-backend.myicpc.live/";
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + localStorage.getItem("token")
        });
    }
    ConfigurationService.prototype.getConfigInfo = function () {
        // http.setRequestHeader("Authorization", "Bearer " + );
        return this.http.get(this.CMS_URL + "categoryInfo");
    };
    ConfigurationService.prototype.getConfigurations = function () {
        return this.http.get(this.CMS_URL + "configuration", { headers: this.headers });
    };
    ConfigurationService.prototype.createConfig = function (config) {
        return this.http.post(this.CMS_URL + "/configuration", config);
    };
    ConfigurationService.prototype.createExam = function (exam) {
        console.log(exam);
        return this.http.post(this.EMS_URL + "exam", exam);
    };
    ConfigurationService.prototype.isEmailValid = function (email) {
        return this.http.get(this.CMS_URL + "exam/" + email, { headers: this.headers });
    };
    ConfigurationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root',
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], keycloak_angular__WEBPACK_IMPORTED_MODULE_2__["KeycloakService"]])
    ], ConfigurationService);
    return ConfigurationService;
}());



/***/ }),

/***/ "./src/app/exam-assignment/exam-assignment.component.html":
/*!****************************************************************!*\
  !*** ./src/app/exam-assignment/exam-assignment.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"columns\">\n  <div class=\"column is-10-mobile is-6-tablet is-offset-3 is-offset-1-mobile\">\n    <form [formGroup]=\"reviewForm\" (ngSubmit)=\"onSubmit()\" novalidate>\n\n\n      <div class=\"field\">\n        <label class=\"label\">Examinee</label>\n        <div class=\"control\">\n          <input class=\"input\" type=\"text\" placeholder=\"Examinee\" formControlName=\"examinee\">\n        </div>\n        <p class=\"help is-success\" *ngIf=\"id\">Email is valid</p>\n      </div>\n\n      <div class=\"field\">\n        <label class=\"label\">Configuration</label>\n        <div class=\"control\">\n          <ng-select [items]=\"configurations\"\n                     bindLabel=\"name\"\n                     autofocus\n                     bindValue=\"id\"\n                     placeholder=\"Select Configuration\"\n                     formControlName=\"configurationId\">\n          </ng-select>\n        </div>\n\n      </div>\n\n      <div class=\"field is-grouped\">\n        <div class=\"control\">\n          <button class=\"button is-link\" type=\"submit\" [disabled]=\"!id\">Save</button>\n        </div>\n        <div class=\"control\">\n          <button class=\"button is-text\" type=\"reset\" (click)=\"revert()\" [disabled]=\"reviewForm.pristine\">Revert</button>\n        </div>\n      </div>\n\n    </form>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/exam-assignment/exam-assignment.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/exam-assignment/exam-assignment.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V4YW0tYXNzaWdubWVudC9leGFtLWFzc2lnbm1lbnQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/exam-assignment/exam-assignment.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/exam-assignment/exam-assignment.component.ts ***!
  \**************************************************************/
/*! exports provided: ExamAssignmentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExamAssignmentComponent", function() { return ExamAssignmentComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _configuration_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../configuration.service */ "./src/app/configuration.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ExamAssignmentComponent = /** @class */ (function () {
    function ExamAssignmentComponent(formBuilder, configuration) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.configuration = configuration;
        this.reviewForm = this.createFormGroup();
        this.configuration.getConfigurations().subscribe(function (data) {
            _this.configurations = data;
        });
        this.reviewForm.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(1000), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["distinctUntilChanged"])()).subscribe(function (data) {
            _this.configuration.isEmailValid(data.examinee).subscribe(function (data2) {
                var email = data2;
                _this.id = email.email;
            });
        });
    }
    ExamAssignmentComponent.prototype.ngOnInit = function () {
    };
    ExamAssignmentComponent.prototype.createFormGroup = function () {
        return new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            examinee: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            configurationId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]()
        });
    };
    ExamAssignmentComponent.prototype.onSubmit = function () {
        var _this = this;
        // Make sure to create a deep copy of the form-model
        var result = Object.assign({}, this.reviewForm.value);
        // Do useful stuff with the gathered data
        //result.examDate = new Date();
        result.examinee = this.id;
        console.log(result);
        this.configuration.createExam(result).subscribe(function (data) {
            console.log(data);
            alert("Assignment created");
            _this.reviewForm.reset();
        }, function (error1) { return console.log(error1); });
    };
    ExamAssignmentComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-exam-assignment',
            template: __webpack_require__(/*! ./exam-assignment.component.html */ "./src/app/exam-assignment/exam-assignment.component.html"),
            styles: [__webpack_require__(/*! ./exam-assignment.component.scss */ "./src/app/exam-assignment/exam-assignment.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _configuration_service__WEBPACK_IMPORTED_MODULE_2__["ConfigurationService"]])
    ], ExamAssignmentComponent);
    return ExamAssignmentComponent;
}());



/***/ }),

/***/ "./src/app/navigation/navigation.component.html":
/*!******************************************************!*\
  !*** ./src/app/navigation/navigation.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar is-link\" role=\"navigation\" aria-label=\"main navigation\" >\n  <!-- style=\"margin-right: 1em; margin-left: 1em;\" -->\n  <div class=\"navbar-brand\">\n    <span class=\"navbar-item\">\n      CS Exam assignment\n    </span>\n    <a role=\"button\" class=\"navbar-burger burger\" aria-label=\"menu\" aria-expanded=\"false\" data-target=\"navbarBasicExample\">\n      <span aria-hidden=\"true\"></span>\n      <span aria-hidden=\"true\"></span>\n      <span aria-hidden=\"true\"></span>\n    </a>\n  </div>\n\n  <div id=\"navbarBasicExample\" class=\"navbar-menu\">\n    <div class=\"navbar-start\">\n      <a class=\"navbar-item\" [routerLink]=\"['/exam']\">\n        Exam assignments\n      </a>\n      <a class=\"navbar-item\" [routerLink]=\"['']\">\n        Configuration creator\n      </a>\n\n\n    </div>\n\n    <div class=\"navbar-end\">\n      <div class=\"navbar-item\">\n        <div class=\"buttons\">\n          <a class=\"button is-light\" href=\"http://ums-demo.myicpc.live\">\n            Profile\n          </a>\n          <a class=\"button is-light\" (click)=\"signout()\">\n            Sign out\n          </a>\n        </div>\n      </div>\n    </div>\n  </div>\n</nav>\n"

/***/ }),

/***/ "./src/app/navigation/navigation.component.scss":
/*!******************************************************!*\
  !*** ./src/app/navigation/navigation.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".is-link {\n  background-color: #1565c0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zdmFjaW5hL2dpdC9hZHZzZXByb2plY3QvY21zLWNsaWVudC9zcmMvYXBwL25hdmlnYXRpb24vbmF2aWdhdGlvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDBCQUF5QixFQUMxQiIsImZpbGUiOiJzcmMvYXBwL25hdmlnYXRpb24vbmF2aWdhdGlvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pcy1saW5re1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTU2NWMwO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/navigation/navigation.component.ts":
/*!****************************************************!*\
  !*** ./src/app/navigation/navigation.component.ts ***!
  \****************************************************/
/*! exports provided: NavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function() { return NavigationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var keycloak_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! keycloak-angular */ "./node_modules/keycloak-angular/fesm5/keycloak-angular.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavigationComponent = /** @class */ (function () {
    function NavigationComponent(keycloak) {
        this.keycloak = keycloak;
    }
    NavigationComponent.prototype.ngOnInit = function () {
    };
    NavigationComponent.prototype.signout = function () {
        this.keycloak.logout();
    };
    NavigationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-navigation',
            template: __webpack_require__(/*! ./navigation.component.html */ "./src/app/navigation/navigation.component.html"),
            styles: [__webpack_require__(/*! ./navigation.component.scss */ "./src/app/navigation/navigation.component.scss")]
        }),
        __metadata("design:paramtypes", [keycloak_angular__WEBPACK_IMPORTED_MODULE_1__["KeycloakService"]])
    ], NavigationComponent);
    return NavigationComponent;
}());



/***/ }),

/***/ "./src/app/utils/app-init.ts":
/*!***********************************!*\
  !*** ./src/app/utils/app-init.ts ***!
  \***********************************/
/*! exports provided: initializer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initializer", function() { return initializer; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};

function initializer(keycloak) {
    var _this = this;
    return function () {
        return new Promise(function (resolve, reject) { return __awaiter(_this, void 0, void 0, function () {
            var error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, keycloak.init({
                                config: _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].keycloak,
                                initOptions: {
                                    onLoad: 'login-required',
                                    checkLoginIframe: false
                                },
                                bearerExcludedUrls: []
                            })];
                    case 1:
                        _a.sent();
                        resolve();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        reject(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); });
    };
}


/***/ }),

/***/ "./src/app/wlayout/wlayout.component.html":
/*!************************************************!*\
  !*** ./src/app/wlayout/wlayout.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<app-navigation></app-navigation>\n\n<div class=\"columns content\">\n\n    <main class=\"column is-10 is-offset-1\" style=\"margin-top: 3em;\">\n        <router-outlet></router-outlet>\n    </main>\n</div>\n"

/***/ }),

/***/ "./src/app/wlayout/wlayout.component.scss":
/*!************************************************!*\
  !*** ./src/app/wlayout/wlayout.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dsYXlvdXQvd2xheW91dC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/wlayout/wlayout.component.ts":
/*!**********************************************!*\
  !*** ./src/app/wlayout/wlayout.component.ts ***!
  \**********************************************/
/*! exports provided: WlayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WlayoutComponent", function() { return WlayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var WlayoutComponent = /** @class */ (function () {
    function WlayoutComponent() {
    }
    WlayoutComponent.prototype.ngOnInit = function () {
    };
    WlayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-wlayout',
            template: __webpack_require__(/*! ./wlayout.component.html */ "./src/app/wlayout/wlayout.component.html"),
            styles: [__webpack_require__(/*! ./wlayout.component.scss */ "./src/app/wlayout/wlayout.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], WlayoutComponent);
    return WlayoutComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var keycloakConfig = {
    url: 'http://ec2-3-87-186-137.compute-1.amazonaws.com:8080/auth',
    realm: 'UserManagement',
    clientId: 'cms-frontend'
};
var environment = {
    production: false,
    keycloak: keycloakConfig,
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/svacina/git/advseproject/cms-client/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map